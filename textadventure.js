// Public functions
var setGlobalVar = function(varName, varValue)  {
	gameState.vars[getGlobalVarName(varName)] = varValue;
}

var initGlobalVar = function(varName, varValue)  {
	if (gameState.vars[getGlobalVarName(varName)] === undefined) {
		setGlobalVar(varName, varValue);
	}
}

var getGlobalVar = function(varName) {
	var value = gameState.vars[getGlobalVarName(varName)];
	if (value === undefined) {
		alert('Tried to get value of global variable "' + varName + '" which was not initialized.');
	}
	return value;
}

var setLocalVar = function(varName, varValue)  {
	gameState.vars[getLocalVarName(varName)] = varValue;
}

var initLocalVar = function(varName, varValue)  {
	if (gameState.vars[getLocalVarName(varName)] === undefined) {
		setLocalVar(varName, varValue);
	}
}

var getLocalVar = function(varName) {
	var value = gameState.vars[getLocalVarName(varName)];
	if (value === undefined) {
		alert('Tried to get value of local variable "' + varName + '" of scene "' + gameState.currentScene + '" which was not initialized.');
	}
	return value;
}

var refreshBool = function(varName) {
	setLocalVar(varName, false);
	refreshView();
	setLocalVar(varName, true);
}

var addToInventory = function(itemName) {
	var existingIndex = gameState.inventory.indexOf(itemName);
	if (existingIndex == -1) {
		gameState.inventory.push(itemName);
		gameState.inventoryModified = true;
	}
}

var removeFromInventory = function(itemName) {
	var existingIndex = gameState.inventory.indexOf(itemName);
	if (existingIndex != -1) {
		gameState.inventory.splice(existingIndex, 1);
		gameState.inventoryModified = true;
	}
}

var hasInInventory = function(itemName) {
	var existingIndex = gameState.inventory.indexOf(itemName);
	return existingIndex != -1;
}

var usingItem = function(itemName) {
	return gameState.selectedItem === itemName;
}

var usingAnyItem = function() {
	return gameState.selectedItem !== null;
}

var goToScene = function(sceneName) {
	var nextSceneElement = $('.scene#' + sceneName);
	if (nextSceneElement.length === 0) {
		alert('Tried to go to scene "' + sceneName + '" which does not exist.');
		return;
	}
	
	gameState.nextScene = sceneName;
}

var saveGame = function() {
	if (Modernizr.localstorage) {
		localStorage.setItem('gameState', JSON.stringify(gameState));
	}
}	

var loadGame = function() {
	if (Modernizr.localstorage) {
		var storedState = localStorage.getItem('gameState');
		if (storedState) {
			var oldCurrentScene = gameState.currentScene;
			gameState = JSON.parse(storedState);
			var newCurrentScene = gameState.currentScene;
			gameState.currentScene = oldCurrentScene;
			gameState.inventoryModified = true;
			goToScene(newCurrentScene);
		}
	}
}

var hasSavedGame = function() {
	return Modernizr.localstorage && localStorage.getItem('gameState');
}

// Private functions

var gameState = {
	vars : {},
	currentScene : 'start',
	nextScene : 'start',
	inventory : new Array(),
	selectedItem : null,
	inventoryModified : false
};

var getLocalVarName = function(varName) {
	return 'local_' + gameState.currentScene + '_' + varName;
}

var getGlobalVarName = function(varName) {
	return 'global_' + varName;
}

var refreshInventory = function() {
	if (gameState.inventoryModified) {
		gameState.inventoryModified = false;
	} else {
		return;
	}
			
	var inventoryElement = $('p#inventory');
	inventoryElement.fadeOut('slow', function() {
		inventoryElement.empty();	
		gameState.selectedItem = null;
		if (gameState.inventory.length > 0) {
			inventoryElement.append("You are carrying ");
		}
		for (var i=0; i < gameState.inventory.length; i++) {
			var itemElement = document.createElement('span');
			itemElement.setAttribute('class', 'inventoryItem');
			itemElement.setAttribute('id', gameState.inventory[i]);
			itemElement.appendChild(document.createTextNode(gameState.inventory[i]));
			$(itemElement).click(function (event) {
				if ($(":animated").length > 0) return false;
				event.stopPropagation();
				clearSelectedItem();
				gameState.selectedItem = $(this).attr('id');
				$(this).css({'text-decoration': 'underline'});
			});
			inventoryElement.append(itemElement);	
			if (i == gameState.inventory.length - 2) {
				inventoryElement.append(' and ');
			}
			else if (i == gameState.inventory.length - 1) {
				inventoryElement.append('.');
			}
			else {
				inventoryElement.append(', ');
			}
		}
		inventoryElement.fadeIn('slow');
	});
}

var clearSelectedItem = function() {	
	gameState.selectedItem = null;
	$('p#inventory').find('.inventoryItem').css({'text-decoration': 'none'});	
}

var refreshView = function() {
	var currentSceneElement = $('.scene#' + gameState.currentScene);

	if (gameState.nextScene !== null) {
		var nextSceneElement = $('.scene#' + gameState.nextScene);
		gameState.currentScene = gameState.nextScene;
		gameState.nextScene = null;
		eval(nextSceneElement.attr("execute"));
		nextSceneElement.find("span.conditional").each(function (n) {
			var shouldShow = eval($(this).attr("test"));
			if (shouldShow) $(this).show(); else $(this).hide();
		});	
		currentSceneElement.fadeOut('slow', function() {
			nextSceneElement.fadeIn('slow');
		});
	} else {
		currentSceneElement.find("span.conditional").each(function (n) {
			var shouldShow = eval($(this).attr("test"));
			if (shouldShow) $(this).fadeIn('slow'); else $(this).fadeOut('slow');
		});
	}
	refreshInventory();
}

$("span.clickable").click(function (event) {
	if ($(":animated").length > 0) return false;
	eval($(this).attr("execute"));
	refreshView();
	saveGame();
});

$('html').click(function() {
	clearSelectedItem();
});

refreshView();

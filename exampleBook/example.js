var start_f0 = function() {}
var start_f1 = function() { goToScene('home'); }
var start_f2 = function() { loadGame(); }
var start_f3 = function() { return hasSavedGame(); }
var home_f0 = function() { addToInventory('id'); setLocalVar('triedToOpenDoor', false); }
var home_f1 = function() { 
	if (usingItem('a key')) {
		goToScene('forest');
	} else {
		refreshBool('triedToOpenDoor', true);
	}
 }
var home_f2 = function() { return !hasInInventory('a key'); }
var home_f3 = function() { if (!hasInInventory('a rat')) addToInventory('a rat'); else addToInventory('a key'); }
var home_f4 = function() { return hasInInventory('a rat'); }
var home_f5 = function() { return getLocalVar('triedToOpenDoor'); }
var forest_f0 = function() {}
var forest_f1 = function() { refreshBool('lookedAtTrees', true); }
var forest_f2 = function() { return getLocalVar('lookedAtTrees'); }

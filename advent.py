import sys
import re

usage_text = """
Usage: advent.py <input filename without extension>

'.txt' extension is assumed for input file.
For example, to convert test1.txt to test1.html: advent.py test1"""

if len(sys.argv) != 2:
	print(usage_text)
	exit();

input_file_name = sys.argv[1] + ".txt"
output_html_file_name = sys.argv[1] + ".html"
output_js_file_name = sys.argv[1] + ".js"

output_prefix = """<!DOCTYPE html>
<html>

<!--This is an auto-generated file. DO NOT EDIT. -->

<head>
	<link rel="stylesheet" type="text/css" href="../textadventure.css" media="screen" />
	<script src="../jquery-2.0.0.min.js"></script>
	<script src="../modernizr.custom.06985.js"></script>
	<script src="{0}"></script>
</head>

<body>""".format(output_js_file_name)

output_suffix = """	

<p id="inventory"></p>

<script src="../textadventure.js"></script>

</body>

</html>"""

print("Opening input file " + input_file_name + "...")
with open(input_file_name, encoding='utf-8') as input_file:
	print("Reading input file into buffer...");
	input_data = input_file.read()
	output_html = output_prefix;
	output_js = "";
	
	print("Processing scenes...")
	patternScene = re.compile(r"""
		\s*(.+?)\s*		# Scene reference name (trim whitespace)	
		\|				# A separator
		\s*(.+?)\s*		# Image filename (trim whitespace)
		\|				# A separator
		\s*(.+?)\s*		# Scene title (trim whitespace)
		\|				# A separator
		\s*(\d+)\s*		# Init function number
		\-\-\-+			# At least three dashes
		(.+?)			# The scene body
		\-\-\-+			# At least three dashes
	""", re.DOTALL | re.VERBOSE)
	
	sceneNames = set()
	for matchScene in patternScene.finditer(input_data):
		sceneName = matchScene.group(1)
		sceneImage = matchScene.group(2)
		sceneTitle = matchScene.group(3)
		sceneInitFunction = matchScene.group(4)
		sceneContents = matchScene.group(5)
		
		assert sceneName not in sceneNames, 'Scene ' + sceneName + ' appears more than once. Scene names must be unique!'
		sceneNames.add(sceneName)
		print("\nProcessing scene '" + sceneName + "'...")
		functionReferences = set( sceneInitFunction )
		print("Processing conditionals...")
		patternConditional = re.compile(r"""
			\[ 			# Opening bracket
			([^[]+?)	# A string without an opening bracket (matches the innermost)
			\] 			# Closing bracket
			(\d+)		# Associated function number
		""", re.DOTALL | re.VERBOSE)
		iteration = 1;
		while (patternConditional.search(sceneContents) != None):
			print("Iteration " + str(iteration) + "...")
			for match in patternConditional.finditer(sceneContents):
				functionReferences.add(match.group(2))
			sceneContents = patternConditional.sub(r'<span class="conditional" test="{0}_f\2()">\1</span>'.format(sceneName), sceneContents)
			iteration = iteration + 1
		
		print("Processing clickables...")
		patternClickable = re.compile(r"""
			\{				# Opening bracket
			([^{]+?)		# A string without an opening bracket (matches the innermost)
			\}				# Closing bracket
			(\d+)			# Associated function number
		""", re.DOTALL | re.VERBOSE)
		for match in patternClickable.finditer(sceneContents):
			functionReferences.add(match.group(2))
		sceneContents = patternClickable.sub(r'<span class="clickable" execute="{0}_f\2()">\1</span>'.format(sceneName), sceneContents)
		
		print("Processing functions...")
		functionNumbers = set()
		patternFunction = re.compile(r"""
			(\d+)			# Function number
			\[				# Opening bracket
			(.*?)			# Javascript code
			\]				# Closing bracket
		""", re.DOTALL | re.VERBOSE)
		for match in patternFunction.finditer(sceneContents):
			assert match.group(1) not in functionNumbers, 'Function number ' + match.group(1) + ' is defined more than once.'
			functionNumbers.add(match.group(1))
			functionName = sceneName + "_f" + match.group(1)
			functionContents = match.group(2)
			output_js += 'var ' + functionName + ' = function() {' + functionContents + '}\n'
		sceneContents = patternFunction.sub('', sceneContents).strip()
		
		undefinedFunctions = functionReferences - functionNumbers
		unusedFunctions = functionNumbers - functionReferences
		assert len(undefinedFunctions) == 0, 'There are undefined references to functions: ' + str(undefinedFunctions)
		assert len(unusedFunctions) == 0, 'There are unused functions: ' + str(unusedFunctions)
		
		output_html += r"""

<div class="scene" id="{0}" execute="{0}_f{1}()">
<h1>{2}</h1>
<img class="side" src="{3}"/>
{4}
</div>
		""".format(sceneName, sceneInitFunction, sceneTitle, sceneImage, sceneContents)
	
	output_html += output_suffix
	
	print("Opening output html file " + output_html_file_name + "...")
	with open(output_html_file_name, mode='w', encoding='utf-8') as output_html_file:
		print("Writing into output html file...")
		output_html_file.write(output_html)
		
	print("Opening output js file " + output_js_file_name + "...")
	with open(output_js_file_name, mode='w', encoding='utf-8') as output_js_file:
		print("Writing into output js file...")
		output_js_file.write(output_js)

	print("Done.")
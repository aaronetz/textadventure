Text Adventure
--------------

Files:

- exampleBook/* : a book for reference
- bookpage.jpg : the generic book background
- textadventure.css : the CSS file for the books
- textadventure.js : the JavaScript file containing generic book code
- advent.py: a compiler from a special text format to the html book

Function reference:

- setGlobalVar(varName, varValue) : Sets a value of a global variable (that is accessible from all scenes).
- getGlobalVar(varName) : Gets a value of a global variable.
- setLocalVar(varName, varValue) : Sets a value of a local variable (that is accessible from the current scene only).
- getLocalVar(varName) : Gets a value of a local variable.
- refreshBool(varName) : Sets a boolean variable to false and then true, refreshing the view each time.
- addToInventory(itemName) : Adds the given item to the inventory
- removeFromInventory(itemName) : Removes the given item from the inventory
- hasInInventory(itemName) : returns whether the given item is in the inventory
- usingItem(itemName) : returns whether the given item is currently being used (selected)
- goToScene(sceneName) : switches to the given scene
- saveGame() : saves the current game
- loadGame() : load the saved game
- hasSavedGame(): returnes whether there is an existing saved game
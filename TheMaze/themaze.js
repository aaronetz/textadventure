var start_f0 = function() {}
var start_f1 = function() { goToScene('entrance'); }
var start_f2 = function() { loadGame(); }
var start_f3 = function() { return hasSavedGame(); }
var entrance_f0 = function() {
	initLocalVar('clickedOnForest', false);
	initLocalVar('clickedOnWall', false);
	addToInventory('a sword');
	addToInventory('a scroll');
 }
var entrance_f1 = function() { refreshBool('clickedOnForest'); }
var entrance_f2 = function() { return getLocalVar('clickedOnForest'); }
var entrance_f3 = function() {
	if (usingItem('a sword')) {
		removeFromInventory('a sword');
		addToInventory('a bent sword');
	} else if (!usingAnyItem()) {
		refreshBool('clickedOnWall');
	}
 }
var entrance_f4 = function() { return getLocalVar('clickedOnWall'); }
var entrance_f5 = function() { goToScene('room_c1'); }
var room_a1_f0 = function() {}
var room_a1_f1 = function() { goToScene('room_a2'); }
var room_a1_f2 = function() { goToScene('room_b1'); }
var room_a2_f0 = function() {}
var room_a2_f1 = function() { goToScene('room_a3'); }
var room_a2_f2 = function() { goToScene('room_b2'); }
var room_a2_f3 = function() { goToScene('room_a1'); }
var room_a3_f0 = function() {}
var room_a3_f1 = function() { goToScene('room_a4'); }
var room_a3_f2 = function() { goToScene('room_b3'); }
var room_a3_f3 = function() { goToScene('room_a2'); }
var room_a4_f0 = function() {}
var room_a4_f1 = function() { goToScene('room_a5'); }
var room_a4_f2 = function() { goToScene('room_a3'); }
var room_a5_f0 = function() {}
var room_a5_f1 = function() { goToScene('room_a4'); }
var room_a5_f2 = function() { goToScene('room_b5'); }
var room_b1_f0 = function() {}
var room_b1_f1 = function() { goToScene('room_a1'); }
var room_b1_f2 = function() { goToScene('room_b2'); }
var room_b2_f0 = function() {}
var room_b2_f1 = function() { goToScene('room_a2'); }
var room_b2_f2 = function() { goToScene('room_b3'); }
var room_b2_f3 = function() { goToScene('room_b1'); }
var room_b3_f0 = function() {}
var room_b3_f1 = function() { goToScene('room_a3'); }
var room_b3_f2 = function() { goToScene('room_b4'); }
var room_b3_f3 = function() { goToScene('room_b2'); }
var room_b4_f0 = function() {}
var room_b4_f1 = function() { goToScene('room_b3'); }
var room_b4_f2 = function() { goToScene('room_c4'); }
var room_b5_f0 = function() {}
var room_b5_f1 = function() { goToScene('room_a5'); }
var room_b5_f2 = function() { goToScene('room_c5'); }
var room_c1_f0 = function() { initLocalVar('firstTime', true); }
var room_c1_f1 = function() { return getLocalVar('firstTime') === true; }
var room_c1_f2 = function() { return getLocalVar('firstTime') === false; }
var room_c1_f3 = function() { setLocalVar('firstTime', false); goToScene('room_c2'); }
var room_c2_f0 = function() {}
var room_c2_f1 = function() { goToScene('room_c1'); }
var room_c2_f2 = function() { goToScene('room_d2'); }
var room_c3_f0 = function() {}
var room_c4_f0 = function() {}
var room_c4_f1 = function() { goToScene('room_b4'); }
var room_c4_f2 = function() { goToScene('room_c3'); }
var room_c5_f0 = function() {}
var room_c5_f1 = function() { goToScene('room_b5'); }
var room_c5_f2 = function() { goToScene('room_d5'); }
var room_d1_f0 = function() {}
var room_d1_f1 = function() { goToScene('room_e1'); }
var room_d2_f0 = function() {}
var room_d2_f1 = function() { goToScene('room_c2'); }
var room_d2_f2 = function() { goToScene('room_d3'); }
var room_d2_f3 = function() { goToScene('room_e2'); }
var room_d3_f0 = function() {}
var room_d3_f1 = function() { goToScene('room_d4'); }
var room_d3_f2 = function() { goToScene('room_d2'); }
var room_d4_f0 = function() {}
var room_d4_f1 = function() { goToScene('room_d5'); }
var room_d4_f2 = function() { goToScene('room_d3'); }
var room_d5_f0 = function() {}
var room_d5_f1 = function() { goToScene('room_e5'); }
var room_d5_f2 = function() { goToScene('room_d4'); }
var room_d5_f3 = function() { goToScene('room_c5'); }
var room_e1_f0 = function() {}
var room_e1_f1 = function() { goToScene('room_e2'); }
var room_e1_f2 = function() { goToScene('room_d1'); }
var room_e2_f0 = function() {}
var room_e2_f1 = function() { goToScene('room_e3'); }
var room_e2_f2 = function() { goToScene('room_d2'); }
var room_e3_f0 = function() {}
var room_e3_f1 = function() { goToScene('room_e2'); }
var room_e4_f0 = function() {}
var room_e4_f1 = function() { goToScene('room_e5'); }
var room_e5_f0 = function() {}
var room_e5_f1 = function() { goToScene('room_e4'); }
var room_e5_f2 = function() { goToScene('room_d5'); }
